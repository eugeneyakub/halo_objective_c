//
//  RLViewController.m
//  koko
//
//  Created by Eugene on 13/01/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import "RLViewController.h"
#import "RLHalo.h"
@import CoreLocation;

@interface RLViewController () <CLLocationManagerDelegate>
@property (nonatomic, strong) RLHalo *halo;
@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic, strong) NSArray *encodedLocations;
@property (nonatomic, assign) double lon;
@property (nonatomic, assign) double lat;
@property (nonatomic, strong) NSString *action;
@property (nonatomic, strong) NSString *color;
@property (nonatomic, strong) NSString *locId;

@property Firebase* rootColorNode ;
@property Firebase* rootMeetingsNode ;
@property Firebase* rootLocationsNode;
@property Firebase* currentLocationsNode;
@property Firebase* someLocationsNode;
@property (nonatomic, strong) NSMutableArray *firebaseLocations;
@property Firebase* currentMeetingsNode;
@property Firebase* crowdNode;
@property Firebase* stateNode;
@property Firebase* breakNode;
@property NSString* currentState;
@property NSString* meetingId;



@property NSString *userId;
@property NSString *avatarUrl;
@property NSString *anotherUserId;
@property BOOL isMaster;

@end

@implementation RLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.userId = [self genRandStringLength:15];
    self.avatarUrl = [self genRandStringLength:4];
    self.halo = [RLHalo new];
    [self initFirebase];
    [self.locationManager startUpdatingLocation];
    
    

}

NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

-(NSString *) genRandStringLength: (int) len {
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    return randomString;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *location = [locations lastObject];
    NSLog(@"%f %f %f %f", location.coordinate.latitude, location.coordinate.longitude, location.verticalAccuracy, location.horizontalAccuracy);
    

    self.lon = location.coordinate.longitude;
    self.lat = location.coordinate.latitude;
    
    //[self.halo get_allWithArc:140 longitude:self.lon latitude:self.lat];
    [self.halo get_allWithArc:25 longitude:61.40 latitude:55.16];
    if (!self.encodedLocations)
        self.encodedLocations = [self.halo get_locations];
    
    [self.locationManager stopUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"%@", error);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initFirebase{
    self.rootLocationsNode = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://beens.firebaseIO.com/locations"]];
    self.rootMeetingsNode = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://beens.firebaseIO.com/meetings"]];
   // [self handleUserPresense];
}


- (BOOL) isExistNode : (Firebase *)node{
    __block BOOL exist = NO;
    [node observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if (snapshot.value == [NSNull null]) {
            NSLog(@"hole node");
            
            self.isMaster = YES;
            [self invokeNewMeeting];
        } else{
            NSLog(@"%@ -> %@", snapshot.name, snapshot.value);
            
            
            exist = YES;
            self.meetingId = snapshot.value;
            
            
            self.isMaster = NO;
            [self injectIntoExistedMeeting];
        }
    }];
    return exist;
}

- (BOOL) isExistNodeArr : (NSArray *)arr{
    __block BOOL exist = NO;
    __block NSInteger iterator = 0;
    __block NSInteger iterator2 = 0;
    NSInteger _count = [arr count];
    for (Firebase *fl in arr) {
    [fl observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        iterator2 ++;
        if (snapshot.value == [NSNull null]) {
            NSLog(@"hole node");
            iterator++;
            self.isMaster = YES;
        } else{
            NSLog(@"%@ -> %@", snapshot.name, snapshot.value);
            exist = YES;
            self.meetingId = snapshot.value;
            self.isMaster = NO;
        }
        if (iterator == _count) {                               //абсолютно все проверяемые ноды пустые
            self.isMaster = YES;
            [self invokeNewMeeting];
            return ;
        }
        if (iterator2 == _count) {                              //видимо не все проверяемые ноды пустые
            self.isMaster = NO;
            [self injectIntoExistedMeeting];
            return;
        }
    }];
    }
    return exist;
}

- (void) handleCrowdNode{
     self.crowdNode = [self.currentMeetingsNode childByAppendingPath:@"crowd"];
    [self.crowdNode observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
        NSString *userInfo = snapshot.name;
        
        NSLog(@"User added %@ ", userInfo);
    }];
    [self.crowdNode observeEventType:FEventTypeChildRemoved withBlock:^(FDataSnapshot *snapshot) {
        NSString *userInfo = snapshot.name;
        
        NSLog(@"User removed %@ ", userInfo);
    }];
}

//создаем новый митинг: мы - хост
- (void) invokeNewMeeting{
    // делаем push
    self.currentMeetingsNode = [self.rootMeetingsNode childByAutoId];
    // записываем ключ от пуша (митинг id) в цвет
    for (Firebase *fl in self.firebaseLocations) {
        [fl setValue:[self.currentMeetingsNode name]];
    }
    // записываем начинку пуша
    [self.currentMeetingsNode setValue:@{@"master": [NSString stringWithFormat:@"%@:%@", self.userId, self.avatarUrl ], @"crowd":@{}, @"state": @"init", @"break": @{}}];
    
    self.stateNode = [self.currentMeetingsNode childByAppendingPath:@"state"];
    [self.stateNode  observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if(snapshot.value == [NSNull null]) {
            [self.stateNode removeAllObservers];
        } else {
            self.currentState = snapshot.value;
        }
    }];
    
    [self handleCrowdNode];
}


//прикрепляемся к существующему митингу: мы не хост
- (void) injectIntoExistedMeeting{
    // находим конкретный митинг в писке
    self.currentMeetingsNode = [self.rootMeetingsNode childByAppendingPath:self.meetingId];
    NSLog(@"%@", self.currentMeetingsNode);
    
    //записываме в crowd userId
    Firebase *crowdMemeberNode = [self.currentMeetingsNode childByAppendingPath:[NSString stringWithFormat: @"crowd/%@:%@", self.userId, self.avatarUrl]];
    [crowdMemeberNode setValue:@"ios 7"];
    
    self.stateNode = [self.currentMeetingsNode childByAppendingPath:@"state"];
    [self.stateNode  observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if(snapshot.value == [NSNull null]) {
            [self.stateNode removeAllObservers];
        } else {
            self.currentState = snapshot.value;
            if (!self.isMaster && [self.currentState isEqualToString:@"onAir"]) {
                NSLog(@"invoke connected from slave because state became onAir");
               // [self invokeConnectedViewController];
            }
        }
    }];
    
     [self handleCrowdNode];
}


- (void) handleUntap{
    // state init ----> state non-init ----> remove meetingId from color
  //  [self.currentColorNode removeValue];

    if ([self.currentState isEqualToString:@"broken"]) {
        NSLog(@"broken state");
        return;
    }
    [self.crowdNode removeAllObservers];
    if (self.isMaster) {
        // соединение создано
        for (Firebase *fl  in self.firebaseLocations) {
            [fl removeValue];
        }
        
        self.crowdNode = [self.currentMeetingsNode childByAppendingPath:@"crowd"];
        [self.crowdNode observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            if (snapshot.value == [NSNull null]) {
                [self.currentMeetingsNode updateChildValues:@{@"state": @"closed"} withCompletionBlock:^(NSError *error, Firebase *ref) {
                    NSLog(@"host change state to %@", self.currentState);
                }];
            } else {
                [self.currentMeetingsNode updateChildValues:@{@"state": @"onAir"} withCompletionBlock:^(NSError *error, Firebase *ref) {
                    NSLog(@"host change state to %@", self.currentState);
                    NSLog(@"invoke connected from host because he made onAir");
                   // [self invokeConnectedViewController];
                }];
            }
        }];
        
        
        
    } else {
        
        if ([self.currentState isEqualToString:@"init"]) {
            //просто очистить данные
            
            Firebase *crowdMemeberNode = [self.currentMeetingsNode childByAppendingPath:[NSString stringWithFormat: @"crowd/%@:%@", self.userId, self.avatarUrl]];
            [crowdMemeberNode removeValue];
            
            NSLog(@"disconnect slave: removed his userId from crowd ");
            
//            NSLog(@"current state is %@ so slave will change state to broken", self.currentState);
//            
//            self.breakNode = [self.currentMeetingsNode childByAppendingPath:@"break"];
//            Firebase* currentBreakNode = [self.breakNode childByAutoId];
//            [currentBreakNode setValue:[NSString stringWithFormat:@"slave with id %@ untap button before host created connection", self.userId]];
//            
//            [self.currentMeetingsNode updateChildValues:@{@"state": @"broken"} withCompletionBlock:^(NSError *error, Firebase *ref) {
//                NSLog(@"state have been changed to %@ because slave untaped", self.currentState);
//            }];
            
            
        } else if ([self.currentState isEqualToString:@"onAir"]){
            NSLog(@"slave can untap because state is %@ ", self.currentState);
        } else {
            NSLog(@"wrong state is %@ ", self.currentState);
        }
        
    }
    
}


- (IBAction)blueClickStarted:(id)sender {
    self.color = @"blue";
    self.action = @"food";
    
    self.firebaseLocations = [NSMutableArray array];
    for (RLLocation *loc in self.encodedLocations) {
        NSString *path = [NSString stringWithFormat: @"%li:%li:%@:%@", (long)loc.halo_id, (long)loc.sector_id, self.color, self.action];

        Firebase* someLocationsNode = [self.rootLocationsNode childByAppendingPath:path];

        [self.firebaseLocations addObject:someLocationsNode];
        //[self.someLocationsNode setValue:@"koko"];
    }
    
    [self isExistNodeArr:self.firebaseLocations];

    //мы мастеры: вносим в locations идентификаторmeetings
    
}

- (IBAction)blueClickEnded:(id)sender {
    [self handleUntap];
}
@end
