//
//  RLHalo.m
//  koko
//
//  Created by Eugene on 13/01/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import "RLHalo.h"
#import <math.h>
@interface RLHalo()
@property (nonatomic, assign) NSInteger arc;
@property (nonatomic, assign) NSInteger part_count;
@property (nonatomic, assign) NSInteger halo_count;
@property (nonatomic, assign) NSInteger halo_id;
@property (nonatomic, assign) double lon;
@property (nonatomic, assign) double lat;
@property (nonatomic, assign) double r;
@property (nonatomic, assign) double delta;
@property (nonatomic, assign) double lon_min;
@property (nonatomic, assign) double lon_max;
@property (nonatomic, assign) double earth_halo_lengh;
@end

@implementation RLHalo
- (id)init{
    self = [super init];
    if (self){
        self.r                  = 6371000;
        self.earth_halo_lengh   = 2 * M_PI * self.r;
        NSLog(@"earth r: %f", self.earth_halo_lengh);
        self.siblings           = [NSMutableArray array];
    }
    return self;
}

- (id)initWithArc:(NSInteger)arc longitude: (double) lon latitude: (double) lat{
    self = [super init];
    if (self) {
        self.arc                = arc;
        self.lon                = [self radians:lon];
        self.lat                = [self radians:lat];
        self.r                  = 6371000;
        self.earth_halo_lengh   = 2 * M_PI * self.r;
        NSLog(@"earth r: %f", self.earth_halo_lengh);
        self.siblings           = [NSMutableArray array];
    }
    
    return self;
}


- (void) get_halo_id{
    self.part_count = ceil(self.earth_halo_lengh / (self.arc ));    //число частей
    NSLog(@"part\'s count: %ld", (long)self.part_count);
    
    self.halo_count = ceil(self.part_count / 2.0);                    //число колец
    NSLog(@"halo\'s count: %ld" , (long)(self.halo_count));
    
    self.delta =  M_PI  / self.halo_count;                          //угол в радианах
    NSLog(@"new delta: %f", (self.delta));
    
    self.halo_id = floor((M_PI / 2.0 - self.lat) / self.delta);       //номер кольца
    NSLog(@"halo id: %ld", (long)self.halo_id);
    NSLog(@"\n");
}

- (BOOL) is_exist_index: (NSInteger) count id: (NSInteger) _id{
    if (_id < 0 || _id > count - 1)
        return NO;
    return YES;
}

- (BOOL) location_already_exist: (RLLocation *)location{
    if (self.location.halo_id == location.halo_id && self.location.sector_id == location.sector_id)
        return YES;
    for (RLLocation* existed_location in self.siblings) {
        if (existed_location.halo_id == location.halo_id && existed_location.sector_id == location.sector_id)
            return YES;
    }
    return NO;
}

- (NSInteger)handle_indexByCount: (NSInteger) count andIndex: (NSInteger) index{
    if (index > count - 1) {
        index -= count;
    } else if (index < 0)
        index += count;
    return index;
}


- (double) handleLatitude: (double) lat{
    if (lat >= M_PI_2)
        lat =  M_PI_2 - 0.000001;
    else if (lat <= - M_PI_2)
        lat =  -M_PI_2 + 0.000001;
    return lat;
}

- (double) handleLongitude: (double) lon{
    if (lon <= - M_PI)
        lon = M_PI;
    else if (lon >= M_PI)
        lon = M_PI;
    return lon;
}


- (void) get_for_point: (NSInteger) halo_id angle: (float) angle{
    double halo_r = fabs(self.r * sin(M_PI_2 - angle));                     //радиус кольца
    //NSLog(@"earth radius %f \nhalo\'s radius: %f",   (self.r) ,  (halo_r));
    
    NSInteger sector_count = ceil( 2 * M_PI *halo_r / self.arc);               //число секторов в кольце
    NSLog(@"sector\'s count: %ld", (long)(sector_count));
    
    double phi = 2 * M_PI  / sector_count;
    NSLog(@"new phi: %f", (phi));
    
    NSInteger sector_id = floor((M_PI - self.lon) / phi);           //номер сектора
    NSLog(@"sector id: %ld", (long)(sector_id));
    
    self.location = [[RLLocation alloc] initWithHaloId: halo_id sectorId: sector_id];
    
    NSInteger handled_index = [self  handle_indexByCount:sector_count andIndex:sector_id - 1];
    RLLocation* handled_location = [[RLLocation alloc] initWithHaloId: halo_id sectorId: handled_index];
    if (![self location_already_exist:handled_location]) {
        [self.siblings addObject:handled_location];
    }
    handled_index = [self  handle_indexByCount:sector_count andIndex:sector_id + 1];
    handled_location = [[RLLocation alloc] initWithHaloId: halo_id sectorId: handled_index];
    if (![self location_already_exist:handled_location]) {
        [self.siblings addObject:handled_location];
    }
    
    self.lon_min = M_PI - [self handle_indexByCount:sector_count andIndex:self.location.sector_id - 1] * phi;
    self.lon_max = M_PI - [self handle_indexByCount:sector_count andIndex:self.location.sector_id + 2] * phi;
    
    NSLog(@"lon_min: %f   lon_max: %f", self.lon_min, self.lon_max);
}


- (void) get_for_sibling_halos: (NSInteger) halo_id angle: (float) angle{
    NSLog(@"halo_id: %i", (halo_id));
    double halo_r = fabs(self.r * sin(M_PI_2 - angle));                     //радиус кольца
    //NSLog(@"earth radius %f \nhalo\'s radius: %f",   (self.r) ,  (halo_r));
    
    NSInteger sector_count = ceil( 2 * M_PI *halo_r / self.arc);               //число секторов в кольце
    NSLog(@"sector\'s count: %ld", (long)(sector_count));
    
    double phi = 2 * M_PI  / sector_count;
    NSLog(@"new phi: %f", (phi));
    
    NSInteger sector_id = floor((M_PI - self.lon) / phi);           //номер сектора
    NSLog(@"sector! id: %ld", (long)(sector_id));
    
    
    NSInteger sector_id_min = floor((M_PI - self.lon_min) / phi);
    NSInteger sector_id_max = floor((M_PI - self.lon_max) / phi);
    NSLog(@"sector_id_min: %i sector_id_max: %i", sector_id_min, sector_id_max);
    
    if (sector_id_min > sector_id_max){
        for (int i = sector_id_min; i < sector_count; i++){
            RLLocation *handled_location = [[RLLocation alloc] initWithHaloId: halo_id sectorId: i];
            [self.siblings addObject:handled_location];
        }
        for (int i = 0; i <= sector_id_max; i++){
            RLLocation *handled_location = [[RLLocation alloc] initWithHaloId: halo_id sectorId: i];
            [self.siblings addObject:handled_location];
        }
    } else if (sector_id_min <= sector_id_max){
        for (int i = sector_id_min; i <= sector_id_max; i++){
            RLLocation *handled_location = [[RLLocation alloc] initWithHaloId: halo_id sectorId: i];
            [self.siblings addObject:handled_location];
        }
    }
}

- (void) get_sector_and_siblings: (NSInteger) halo_id latitude: (double) lat{
    //lat = [self handleLatitude:lat];
    double halo_r = self.r * sin(M_PI_2 - lat);                     //радиус кольца
    //NSLog(@"earth radius %f \nhalo\'s radius: %f",   (self.r) ,  (halo_r));
    
    NSInteger sector_count = ceil( 2 * M_PI *halo_r / self.arc);               //число секторов в кольце
    NSLog(@"sector\'s count: %ld", (long)(sector_count));
    
    double phi = 2 * M_PI  / sector_count;
    NSLog(@"new phi: %f", (phi));
    
    NSInteger sector_id = floor((M_PI - self.lon) / phi);           //номер сектора
    NSLog(@"sector id: %ld", (long)(sector_id));
    
    
    if (halo_id == self.halo_id)                                    //сектора не соседей
        self.location = [[RLLocation alloc] initWithHaloId: halo_id sectorId: sector_id];
    else
        [self.siblings addObject:[[RLLocation alloc] initWithHaloId: halo_id sectorId: sector_id]];
    
    if ([self is_exist_index:sector_count id:sector_id - 1 ] && ![self location_already_exist:[[RLLocation alloc] initWithHaloId: halo_id sectorId: sector_id -1]])
        [self.siblings addObject:[[RLLocation alloc] initWithHaloId: halo_id sectorId: sector_id -1]];
    else{
        RLLocation* loc = [[RLLocation alloc] initWithHaloId: halo_id sectorId: sector_count -1];
        if (![self location_already_exist:loc])
            [self.siblings addObject:loc];
    }
    
    
    if ([self is_exist_index:sector_count id:sector_id + 1 ] && ![self location_already_exist:[[RLLocation alloc] initWithHaloId: halo_id sectorId: sector_id + 1]])
        [self.siblings addObject:[[RLLocation alloc] initWithHaloId: halo_id sectorId: sector_id + 1]];
    else{
        RLLocation* loc = [[RLLocation alloc] initWithHaloId: halo_id sectorId: 0];
        if (![self location_already_exist:loc])
            [self.siblings addObject:loc];
    }
    
    NSLog(@"");
    
}

- (void) get_allWithArc:(NSInteger)arc longitude: (double) lon latitude: (double) lat{
    self.arc                = arc;
    self.lon                = [self radians:lon];
    self.lat                = [self radians:lat];
    [self get_all];
}



- (void) get_all{
    self.lon = [self handleLongitude:self.lon];
    self.lat = [self handleLatitude:self.lat];
    [self get_halo_id];
    [self get_for_point:self.halo_id angle:self.delta * [self handle_halo_id: self.halo_id] ];
    
    if ([self is_exist_index: self.halo_count id: self.halo_id - 1 ])
        [self get_for_sibling_halos: self.halo_id - 1 angle:self.delta * [self handle_halo_id: self.halo_id]  + self.delta];
    if ([self is_exist_index: self.halo_count id: self.halo_id + 1 ])
        [self get_for_sibling_halos: self.halo_id + 1 angle:self.delta * [self handle_halo_id: self.halo_id]  - self.delta];
//    [self get_sector_and_siblings: self.halo_id latitude: self.delta * self.halo_id];
//    
//    if ([self is_exist_index: self.halo_count id: self.halo_id - 1 ])
//        [self get_sector_and_siblings: self.halo_id - 1 latitude:self.delta * self.halo_id + self.delta];
//    if ([self is_exist_index: self.halo_count id: self.halo_id + 1 ])
//        [self get_sector_and_siblings: self.halo_id + 1 latitude: self.delta * self.halo_id - self.delta];
    
    
    NSLog(@"\nlocation: ");
    NSLog(@"halo_id: %ld sector_id: %ld: ", (long)(self.location.halo_id) , (long)(self.location.sector_id));
    NSLog(@"siblings: ");
    for (RLLocation* sibling in self.siblings)
        NSLog(@"halo_id: %ld sector_id: %ld: ", (long)(sibling.halo_id) , (long)(sibling.sector_id));
        
}

- (NSInteger)handle_halo_id: (NSInteger) halo_index{
    if (halo_index < floor(self.halo_count / 2.0)) {
        halo_index++;
    }
    return halo_index;
}

- (NSArray *) get_locations{
    NSMutableArray *arr = [NSMutableArray arrayWithArray:self.siblings];
    [arr addObject:self.location];
    return arr;
}

- (double) radians:(double) degreesV
{
    return degreesV * M_PI / 180;
};

- (double) degrees:(double) radiansV
{
    return radiansV * 180 / M_PI;
};
@end
