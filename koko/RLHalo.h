//
//  RLHalo.h
//  koko
//
//  Created by Eugene on 13/01/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RLLocation.h"
@interface RLHalo : NSObject
- (id) initWithArc:(NSInteger)arc longitude: (double) lon latitude: (double) lat;
- (void) get_all;
- (void) get_allWithArc:(NSInteger)arc longitude: (double) lon latitude: (double) lat;

- (NSArray *) get_locations;

@property (nonatomic, strong) RLLocation* location;
@property (nonatomic, strong) NSMutableArray* siblings;
@end
