//
//  RLLocation.h
//  koko
//
//  Created by Eugene on 13/01/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RLLocation : NSObject
@property (nonatomic, assign) NSInteger halo_id;
@property (nonatomic, assign) NSInteger sector_id;

- (id) initWithHaloId:(NSInteger)halo_id sectorId: (NSInteger) sector_id;
@end
