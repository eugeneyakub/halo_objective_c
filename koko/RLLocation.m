//
//  RLLocation.m
//  koko
//
//  Created by Eugene on 13/01/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import "RLLocation.h"

@implementation RLLocation
- (id)initWithHaloId:(NSInteger)halo_id sectorId: (NSInteger) sector_id {
    self = [super init];
    if (self) {
        self.halo_id                = halo_id;
        self.sector_id              = sector_id;

    }
    
    return self;
}


- (BOOL)isEqual:(id)object
{
    // MONInteger allows a comparison to NSNumber
    if ([object isKindOfClass:[RLLocation class]]) {
        RLLocation *location = (RLLocation *)object;
        if (self.halo_id != location.halo_id || self.sector_id != location.sector_id)
            return NO;
    }
    else if (![object isKindOfClass:self.class]) {
        return NO;
    }
    return YES;
}

@end
