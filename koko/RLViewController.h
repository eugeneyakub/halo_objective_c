//
//  RLViewController.h
//  koko
//
//  Created by Eugene on 13/01/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
@interface RLViewController : UIViewController
- (IBAction)blueClickStarted:(id)sender;
- (IBAction)blueClickEnded:(id)sender;

@end
