//
//  RLAppDelegate.h
//  koko
//
//  Created by Eugene on 13/01/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
