//
//  kokoTests.m
//  kokoTests
//
//  Created by Eugene on 13/01/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RLHalo.h"
@interface kokoTests : XCTestCase
@property (nonatomic, strong) RLHalo *halo;
@end

@implementation kokoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.halo  = [RLHalo new];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_BigArc_plus180Lon
{
    [self.halo get_allWithArc:5000000 longitude:180 latitude:44];
//    XCTAssertTrue(self.halo.location.halo_id == 1, @"dfvgreg");
    XCTAssertEqualObjects(@(self.halo.location.halo_id), @1, @"wrong halo_id");
    XCTAssertEqualObjects(@(self.halo.location.sector_id), @0, @"wrong sector_id");

    NSArray *arr = [NSArray arrayWithObjects:
                   [[RLLocation alloc] initWithHaloId:1 sectorId:5],
                   [[RLLocation alloc] initWithHaloId:1 sectorId:1],
                   [[RLLocation alloc] initWithHaloId:0 sectorId:0],
                   [[RLLocation alloc] initWithHaloId:0 sectorId:1],
                   [[RLLocation alloc] initWithHaloId:2 sectorId:0],
                   [[RLLocation alloc] initWithHaloId:2 sectorId:7],
                   [[RLLocation alloc] initWithHaloId:2 sectorId:1],
                   nil];
    XCTAssertEqualObjects(self.halo.siblings, arr, @"wrong siblings");

}


- (void)test_BigArc_minus180Lon
{
    [self.halo get_allWithArc:5000000 longitude:-180 latitude:44];

    XCTAssertEqualObjects(@(self.halo.location.halo_id), @1, @"wrong halo_id");
    XCTAssertEqualObjects(@(self.halo.location.sector_id), @0, @"wrong sector_id");
    
    NSArray *arr = [NSArray arrayWithObjects:
                    [[RLLocation alloc] initWithHaloId:1 sectorId:5],
                    [[RLLocation alloc] initWithHaloId:1 sectorId:1],
                    [[RLLocation alloc] initWithHaloId:0 sectorId:0],
                    [[RLLocation alloc] initWithHaloId:0 sectorId:1],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:0],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:7],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:1],
                    nil];
    XCTAssertEqualObjects(self.halo.siblings, arr, @"wrong siblings");
//
}

- (void)test_BigArc_plus90Lon
{
    [self.halo get_allWithArc:5000000 longitude:90 latitude:44];

    XCTAssertEqualObjects(@(self.halo.location.halo_id), @1, @"wrong halo_id");
    XCTAssertEqualObjects(@(self.halo.location.sector_id), @1, @"wrong sector_id");
    
    NSArray *arr = [NSArray arrayWithObjects:
                    [[RLLocation alloc] initWithHaloId:1 sectorId:0],
                    [[RLLocation alloc] initWithHaloId:1 sectorId:2],
                    [[RLLocation alloc] initWithHaloId:0 sectorId:0],
                    [[RLLocation alloc] initWithHaloId:0 sectorId:1],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:2],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:1],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:3],
                    nil];
    XCTAssertEqualObjects(self.halo.siblings, arr, @"wrong siblings");
    //
}

- (void)test_BigArc_minus90Lon
{
    [self.halo get_allWithArc:5000000 longitude:-90 latitude:44];

    XCTAssertEqualObjects(@(self.halo.location.halo_id), @1, @"wrong halo_id");
    XCTAssertEqualObjects(@(self.halo.location.sector_id), @4, @"wrong sector_id");
    
    NSArray *arr = [NSArray arrayWithObjects:
                    [[RLLocation alloc] initWithHaloId:1 sectorId:3],
                    [[RLLocation alloc] initWithHaloId:1 sectorId:5],
                    [[RLLocation alloc] initWithHaloId:0 sectorId:1],
                    [[RLLocation alloc] initWithHaloId:0 sectorId:0],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:6],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:5],
                    [[RLLocation alloc] initWithHaloId:2 sectorId:7],
                    nil];
    XCTAssertEqualObjects(self.halo.siblings, arr, @"wrong siblings");
    //
}

- (void)test_BigArc_minus90Lat
{
    [self.halo get_allWithArc:5000000 longitude:-140 latitude:-90];

    XCTAssertEqualObjects(@(self.halo.location.halo_id), @4, @"wrong halo_id");
    XCTAssertEqualObjects(@(self.halo.location.sector_id), @0, @"wrong sector_id");
    
    NSArray *arr = [NSArray arrayWithObjects:
                    [[RLLocation alloc] initWithHaloId:3 sectorId:4],
                    [[RLLocation alloc] initWithHaloId:3 sectorId:3],
                    [[RLLocation alloc] initWithHaloId:3 sectorId:0],
                    nil];
    XCTAssertEqualObjects(self.halo.siblings, arr, @"wrong siblings");
    //
}


- (void)test_BigArc_plus90Lat
{
    [self.halo get_allWithArc:5000000 longitude:140 latitude:90];
    
    XCTAssertEqualObjects(@(self.halo.location.halo_id), @0, @"wrong halo_id");
    XCTAssertEqualObjects(@(self.halo.location.sector_id), @0, @"wrong sector_id");
    
    NSArray *arr = [NSArray arrayWithObjects:
                    [[RLLocation alloc] initWithHaloId:1 sectorId:0],
                    [[RLLocation alloc] initWithHaloId:1 sectorId:4],
                    [[RLLocation alloc] initWithHaloId:1 sectorId:1],
                    nil];
    XCTAssertEqualObjects(self.halo.siblings, arr, @"wrong siblings");
    //
}

- (void)test_5metersArc
{
    [self.halo get_allWithArc:5 longitude:-114 latitude:44];
    
    XCTAssertEqualObjects(@(self.halo.location.halo_id), @1022993, @"wrong halo_id");
    XCTAssertEqualObjects(@(self.halo.location.sector_id), @4703232, @"wrong sector_id");
    
    NSArray *arr = [NSArray arrayWithObjects:
                    [[RLLocation alloc] initWithHaloId:1022993 sectorId:4703231],
                    [[RLLocation alloc] initWithHaloId:1022993 sectorId:4703233],
                    [[RLLocation alloc] initWithHaloId:1022992 sectorId:4703229],
                    [[RLLocation alloc] initWithHaloId:1022992 sectorId:4703228],
                    [[RLLocation alloc] initWithHaloId:1022992 sectorId:4703230],
                    [[RLLocation alloc] initWithHaloId:1022994 sectorId:4703235],
                    [[RLLocation alloc] initWithHaloId:1022994 sectorId:4703234],
                    [[RLLocation alloc] initWithHaloId:1022994 sectorId:4703236],
                    nil];
    XCTAssertEqualObjects(self.halo.siblings, arr, @"wrong siblings");
    //
}

@end
